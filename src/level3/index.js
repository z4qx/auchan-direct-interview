/* Auchan direct interview Level3 by Fabien Genet */
//I choose to load my cart like it was an user, item by item with addAricle

lib = require('./liblevel3');
io = require('../libio/io');

var data = io.loadData('./data.json', main);

function main(data) {
	//should check data input to be safe
	var store = new lib.Store(data.articles);
	var deliveryFeee = new lib.DeliveryFees(data.delivery_fees);
	var discounts = new lib.Discounts(data.discounts, store);

	//Create empty Cart followings the dataset
	let carts = [];
	for(var i = 0; i < data.carts.length; ++i) {
		carts.push(new lib.Cart(data.carts[i].id, deliveryFeee, discounts));
	}

	//Add article of each cart
	for(var i = 0; i < data.carts.length; ++i) {
		for(var j = 0; j < data.carts[i].items.length; j++) {
			carts[i].addArticle(data.carts[i].items[j].article_id, data.carts[i].items[j].quantity)
		}
	}

	let cartsBill = {carts:[]};
	for(var i = 0; i < carts.length; ++i) {
		cartsBill.carts.push(carts[i].checkout(store));
	}

	io.saveData('./myoutput.json', cartsBill);
}
