module.exports.Discounts = class Discounts {
	constructor(discountsSettings, store) {
		this.discounts = discountsSettings;
		this.store = store;
	}

	getItemPriceWithDiscount(articleId) {
		var articlePrice = this.store.getArticlePrice(articleId);
		if(articlePrice === null) {
			console.log('Unknow item');
			return null;
		}

		for(var i = 0; i < this.discounts.length; i++) {
			if(this.discounts[i].article_id === articleId) {
				if(this.discounts[i].type === 'amount') {
					return articlePrice - this.discounts[i].value;
				}
				else if(this.discounts[i].type === 'percentage') {
					var value = articlePrice * (this.discounts[i].value/100);
					return articlePrice - (articlePrice * (this.discounts[i].value/100));
				}
			}
		}
		return articlePrice;
	}
}

/** The store contain all article of the website
In the real world the store should be filled by a database **/
module.exports.Store = class Store {
	constructor(articles) {
		this.articles = {};
		for(var i = 0 ; i < articles.length; i++){
			this.articles[articles[i].id] = articles[i];
			//delete this.articles[articles[i].id].id;
		}
	}

	articleExist(articleId) {
		if(articleId !== null && articleId >=0)
			if(this.articles[articleId] === null || typeof this.articles[articleId] !== 'undefined')
				return true;
		return false;
	}

	getArticlePrice(articleId) {
		if(this.articleExist(articleId)) {
			return this.articles[articleId].price;
		}
		else
			return null;
	}
}

module.exports.DeliveryFees = class DeliveryFees {
	constructor(settings) {
		this.delivery_fees = settings;
	}

	produceDeliveryFrees(price) {
		for(var i = 0; i < this.delivery_fees.length; ++i) {
		    if (price >= this.delivery_fees[i].eligible_transaction_volume.min_price && (price < this.delivery_fees[i].eligible_transaction_volume.max_price || this.delivery_fees[i].eligible_transaction_volume.max_price === null)) {
				//console.log(this.delivery_fees[i].price);
				return this.delivery_fees[i].price;
			}
		}
	}
}

/** The cart contain the articles that the client selected and want to buy */
module.exports.Cart = class Cart {
	/* @param id int => Unique id */
	/* @param deliveryFeesStore DeliveryFeesStore => instance of the DeliveryFeesStore */
	/* @param discountsStore DiscountsStore => instance of DiscountsStore */
	constructor(id, deliveryFeesStore, discountsStore) { //cart is empty at creation
		this.deliveryFeeeStore = deliveryFeesStore; //should be managed by higher level object
		this.discountsStore = discountsStore;
		this.id=id;
		this.items = [];
		this.total = 0;
		this.delivery_fees = 0;
	}

	addArticle(articleId, quantity) {
		this.items.push({article_id:articleId, quantity:quantity});
	}

	checkout() {
		for(var i = 0; i < this.items.length; ++i) {
			var itemPrice = this.discountsStore.getItemPriceWithDiscount(this.items[i].article_id);
			if(itemPrice !== null) {
				this.total += itemPrice * this.items[i].quantity;
			}
			else
				console.log('Unknow item');
		}

		var deliveryFeee = this.deliveryFeeeStore.produceDeliveryFrees(this.total);
		this.total += deliveryFeee;

		return {id : this.id, total:this.total};
	}
}