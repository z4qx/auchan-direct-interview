/* Auchan direct interview Level2 by Fabien Genet */

lib = require('./liblevel2');
io = require('../libio/io');

var data = io.loadData('./data.json', main);
// I choose to load my cart like it was an user, item by item with addAricle
function main(data) {
	//should check data input to be safe
	var store = new lib.Store(data.articles);
	var deliveryFeee = new lib.DeliveryFees(data.delivery_fees);

	//Create empty Cart followings the dataset
	let carts = [];
	for(var i = 0; i < data.carts.length; ++i) {
		carts.push(new lib.Cart(data.carts[i].id, store, deliveryFeee));
	}

	//Add article of each cart
	for(var i = 0; i < data.carts.length; ++i) {
		for(var j = 0; j < data.carts[i].items.length; j++) {
			carts[i].addArticle(data.carts[i].items[j].article_id, data.carts[i].items[j].quantity)
		}
	}

	let cartsBill = {carts:[]};
	for(var i = 0; i < carts.length; ++i) {
		cartsBill.carts.push(carts[i].checkout(store));
	}

	io.saveData('./myoutput.json', cartsBill);
}
