var expect    = require("chai").expect;
//var converter = require("../app/converter");
lib1 = require('../level1/liblevel1');
lib2 = require('../level2/liblevel2');
lib3 = require('../level3/liblevel3');

io = require('../libio/io');

var data = JSON.parse('{"articles":[{"id":1,"name":"water","price":100},{"id":2,"name":"honey","price":200},{"id":3,"name":"mango","price":400},{"id":4,"name":"tea","price":1000}],"carts":[{"id":1,"items":[{"article_id":1,"quantity":6},{"article_id":2,"quantity":2},{"article_id":4,"quantity":1}]},{"id":2,"items":[{"article_id":2,"quantity":1},{"article_id":3,"quantity":3}]},{"id":3,"items":[]}]}');
describe("io", function() {
    it("Loading data", function(done) {
    	io.loadData('./level1/data.json', function(loadedData) {
    		expect(loadedData.articles[0]).to.deep.equal(data.articles[0]);
	    	done();
    	})
  });
});

describe("level1", function() {
  describe("Store tests", function() {
    it("Store creation", function() {
    	let store = new lib1.Store(data.articles);
    	expect(store).to.be.an('object');
    });

    it("Check articles", function() {
    	//console.log(data.articles);
    	let store = new lib1.Store(data.articles);
    	expect(store).to.be.an('object');
    	expect(store.articleExist('2')).to.be.true;
    	expect(store.articleExist('5')).to.be.false;
    });

    it("Get articles price", function() {
    	//console.log(data.articles);
    	let store = new lib1.Store(data.articles);
    	expect(store).to.be.an('object');
    	expect(store.getArticlePrice('2')).to.equal(200);
    	expect(store.getArticlePrice('5')).to.be.null;
    });
  });

  //Factoring with mock can be usefull
  describe("Cart tests", function() {
    it("Cart creation", function() {
    	let store = new lib1.Store(data.articles);
    	let cart = new lib1.Cart('1', store);

    	expect(store).to.be.an('object');
    	expect(cart).to.be.an('object');
    });

    it("add articles into cart", function() {
    	let store = new lib1.Store(data.articles);
    	let cart = new lib1.Cart('1', store);

    	cart.addArticle(1,2);
    	var testCart = [{'article_id':1, 'quantity':2}];
    	expect(cart.items).to.deep.equal(testCart);
    });

    it("Get articles price", function() {
    	let store = new lib1.Store(data.articles);
    	expect(store).to.be.an('object');
    	expect(store.getArticlePrice('2')).to.equal(200);
    	expect(store.getArticlePrice('5')).to.be.null;
    });

    it("checkout total price", function() {
    	let store = new lib1.Store(data.articles);
    	let cart = new lib1.Cart('1', store);

    	cart.addArticle(2,5);

    	expect(cart.checkout().total).to.equal(1000);
    });
  });
});

describe("level2", function() {
  data = JSON.parse('{"articles":[{"id":1,"name":"water","price":100},{"id":2,"name":"honey","price":200},{"id":3,"name":"mango","price":400},{"id":4,"name":"tea","price":1000}],"carts":[{"id":1,"items":[{"article_id":1,"quantity":6},{"article_id":2,"quantity":2},{"article_id":4,"quantity":1}]},{"id":2,"items":[{"article_id":2,"quantity":1},{"article_id":3,"quantity":3}]},{"id":3,"items":[]}],"delivery_fees":[{"eligible_transaction_volume":{"min_price":0,"max_price":1000},"price":800},{"eligible_transaction_volume":{"min_price":1000,"max_price":2000},"price":400},{"eligible_transaction_volume":{"min_price":2000,"max_price":null},"price":0}]}');
  describe("Store tests", function() {
    it("Store && DeliveryFeee creation", function() {
    	let store = new lib2.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);

    	expect(store).to.be.an('object');
    	expect(deliveryFeee).to.be.an('object');
    });


    it("Check articles", function() {
    	//console.log(data.articles);
    	let store = new lib2.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);

    	expect(store.articleExist('2')).to.be.true;
    	expect(store.articleExist('5')).to.be.false;
    });

    it("Get articles price", function() {
    	//console.log(data.articles);
    	let store = new lib1.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);


    	expect(store).to.be.an('object');
    	expect(store.getArticlePrice('2')).to.equal(200);
    	expect(store.getArticlePrice('5')).to.be.null;
    });
    
  });

  describe("Cart tests", function() {
    it("Cart creation", function() {
    	let store = new lib2.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);

    	let cart = new lib2.Cart('1', store, deliveryFeee);

    	expect(store).to.be.an('object');
    	expect(cart).to.be.an('object');
    });

    it("add articles into cart", function() {
    	let store = new lib2.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);
    	let cart = new lib2.Cart('1', store, deliveryFeee);

    	cart.addArticle(1,2);
    	var testCart = [{'article_id':1, 'quantity':2}];
    	expect(cart.items).to.deep.equal(testCart);
    });

    it("Get articles price", function() {
    	let store = new lib2.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);

    	expect(store).to.be.an('object');
    	expect(store.getArticlePrice('2')).to.equal(200);
    	expect(store.getArticlePrice('5')).to.be.null;
    });

    it("checkout total price", function() {
    	let store = new lib2.Store(data.articles);
    	let deliveryFeee = new lib2.DeliveryFees(data.delivery_fees);
    	let cart = new lib2.Cart('1', store, deliveryFeee);

    	cart.addArticle(1,6);
    	cart.addArticle(2,2);
    	cart.addArticle(4,1);
    	expect(cart.checkout().total).to.equal(2000);

    	let cart2 = new lib2.Cart('2', store, deliveryFeee);

    	cart2.addArticle(2,1);
    	cart2.addArticle(3,3);
    	expect(cart2.checkout().total).to.equal(1800);

    	let cart3 = new lib2.Cart('2', store, deliveryFeee);
    	expect(cart3.checkout().total).to.equal(800);
    });
  });
});


describe("level3", function() {
  data3 = JSON.parse('{"articles":[{"id":1,"name":"water","price":100},{"id":2,"name":"honey","price":200},{"id":3,"name":"mango","price":400},{"id":4,"name":"tea","price":1000},{"id":5,"name":"ketchup","price":999},{"id":6,"name":"mayonnaise","price":999},{"id":7,"name":"fries","price":378},{"id":8,"name":"ham","price":147}],"carts":[{"id":1,"items":[{"article_id":1,"quantity":6},{"article_id":2,"quantity":2},{"article_id":4,"quantity":1}]},{"id":2,"items":[{"article_id":2,"quantity":1},{"article_id":3,"quantity":3}]},{"id":3,"items":[{"article_id":5,"quantity":1},{"article_id":6,"quantity":1}]},{"id":4,"items":[{"article_id":7,"quantity":1}]},{"id":5,"items":[{"article_id":8,"quantity":4}]}],"delivery_fees":[{"eligible_transaction_volume":{"min_price":0,"max_price":1000},"price":800},{"eligible_transaction_volume":{"min_price":1000,"max_price":2000},"price":400},{"eligible_transaction_volume":{"min_price":2000,"max_price":null},"price":0}],"discounts":[{"article_id":2,"type":"amount","value":25},{"article_id":5,"type":"percentage","value":30},{"article_id":6,"type":"percentage","value":30},{"article_id":7,"type":"percentage","value":25},{"article_id":8,"type":"percentage","value":10}]}');
  describe("Store tests", function() {
    it("Store && DeliveryFeee creation", function() {
    	let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);

    	expect(store).to.be.an('object');
    	expect(deliveryFeee).to.be.an('object');
    	expect(discounts).to.be.an('object');
    });

    it("Check articles", function() {
    	//console.log(data.articles);
    	let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);


    	expect(store.articleExist('2')).to.be.true;
    	expect(store.articleExist('7')).to.be.true;
    	expect(store.articleExist('5')).to.be.true;
    	expect(store.articleExist('10')).to.be.false;
    });
    
    it("Get articles price", function() {
    	//console.log(data.articles);
    	let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);

    	expect(store).to.be.an('object');
    	expect(store.getArticlePrice('2')).to.equal(200);
    	expect(store.getArticlePrice('5')).to.be.equal(999);
    	expect(store.getArticlePrice('10')).to.be.null;
    });
    
  });

  
  describe("Cart tests", function() {
    it("Cart creation", function() {
		let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);
    	let cart = new lib3.Cart('1', deliveryFeee, discounts);

    	expect(store).to.be.an('object');
    	expect(cart).to.be.an('object');
    });

    it("add articles into cart", function() {
		let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);

    	let cart = new lib3.Cart('1', deliveryFeee, discounts);

    	cart.addArticle(1,2);
    	var testCart = [{'article_id':1, 'quantity':2}];
    	expect(cart.items).to.deep.equal(testCart);
    });

    it("Get articles price", function() {
		let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);


    	expect(store).to.be.an('object');
    	expect(store.getArticlePrice('2')).to.equal(200);
    	expect(store.getArticlePrice('5')).to.equal(999);
    	expect(store.getArticlePrice('50')).to.null;
    });

    it("checkout total price", function() {
		let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);

    	let cart = new lib3.Cart('1', deliveryFeee, discounts);

    	cart.addArticle(1,6);
    	cart.addArticle(2,2);
    	cart.addArticle(4,1);
    	expect(cart.checkout().total).to.equal(2350);

    	let cart2 = new lib3.Cart('2', deliveryFeee, discounts);

    	cart2.addArticle(2,1);
    	cart2.addArticle(3,3);
    	expect(cart2.checkout().total).to.equal(1775);

    	let cart3 = new lib3.Cart('3', deliveryFeee, discounts);
    	cart3.addArticle(5,1);
    	cart3.addArticle(6,1);
    	expect(cart3.checkout().total).to.equal(1798.6);

    	let cart4 = new lib3.Cart('4', deliveryFeee, discounts);
    	cart4.addArticle(7,1);
    	expect(cart4.checkout().total).to.equal(1083.5);

    	let cart5 = new lib3.Cart('5', deliveryFeee, discounts);
    	cart5.addArticle(8,4);
    	expect(cart5.checkout().total).to.equal(1329.2);
    });
  });


  describe("DeliveryFee test", function() {
    it("unit test deliveryFee", function() {
    	let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);

    	expect(deliveryFeee.produceDeliveryFrees(500)).to.equal(800);
    	expect(deliveryFeee.produceDeliveryFrees(1500)).to.equal(400);
    	expect(deliveryFeee.produceDeliveryFrees(5000)).to.equal(0);
    	expect(deliveryFeee.produceDeliveryFrees(0)).to.equal(800);
    });
   });

   describe("Discount test", function() {
    it("unit test discount", function() {
    	let store = new lib3.Store(data3.articles);
    	let deliveryFeee = new lib3.DeliveryFees(data3.delivery_fees);
		let discounts = new lib3.Discounts(data3.discounts, store);

    	expect(discounts.getItemPriceWithDiscount(2)).to.equal(175);
    	expect(discounts.getItemPriceWithDiscount(6)).to.equal(699.3);
    });
   });

});
