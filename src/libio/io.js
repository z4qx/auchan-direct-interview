
/** Simple IO lib **/


/** Loading function **/
fs = require('fs')
module.exports.loadData = function(path, callback) {
    fs.readFile(path, 'utf8', function (err, data) {
	  if (err) {
	    return console.log(err);
	  }
	  //Should be protect with try catch
	  callback(JSON.parse(data));
	});
};

/** Save function **/
module.exports.saveData = function(path, data) {
	const content = JSON.stringify(data);
	fs.writeFile(path, content, 'utf8', function (err) {
	    if (err) {
	        return console.log(err);
	    }
	    console.log("The file was saved!");
	});
}