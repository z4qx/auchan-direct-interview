/** The store contain all article of the website
In the real world the store should be filled by a database **/
module.exports.Store = class Store {
	constructor(articles) {
		this.articles = {};
		for(var i = 0 ; i < articles.length; ++i){
			this.articles[articles[i].id] = articles[i];
			//delete this.articles[articles[i].id].id;
		}
	}

	articleExist(articleId) {
		if(articleId !== null && articleId >=0)
			if(this.articles[articleId] === null || typeof this.articles[articleId] !== 'undefined')
				return true;
		return false;
	}

	getArticlePrice(articleId) {
		if(this.articleExist(articleId)) {
			return this.articles[articleId].price;
		}
		else
			return null;
	}
}

/** The cart contain the articles that the client selected and want to buy */
module.exports.Cart = class Cart {
	/* @Param id int => Unique id */
	/* @param store Store => instance of the store */
	constructor(id, store) { //cart is empty at creation
		this.store = store; //should be managed by higher level object
		this.id=id;
		this.items = [];
		this.total = 0;
	}

	addArticle(articleId, quantity) {
		this.items.push({article_id:articleId, quantity:quantity});
		//We can also update the total here
	}

	checkout() {
		for(var i = 0; i < this.items.length; ++i) {
			var itemPrice = this.store.getArticlePrice(this.items[i].article_id);
			if(itemPrice !== null) {
				this.total += itemPrice * this.items[i].quantity;
			}
			else
				console.log('Unknow item');
		}

		return {id : this.id, total:this.total};
	}

}
